import json
import datetime
from calendar import timegm
from jwt import encode as jwt_encode
from social_core.tests.backends.oauth import OAuth2Test
from social_core.tests.backends.open_id_connect import OpenIdConnectTestMixin


class LumericalOAuth2Test(OpenIdConnectTestMixin, OAuth2Test):
    backend_path = 'lumerical_oauth2.backend.LumericalOAuth2'
    issuer = 'www.lumerical.com'
    expected_username = 'some_user'

    @property
    def openid_config_body(self):
        """
        Call the backend's hardcoded oidc_config here.
        """
        return json.dumps(self.backend.oidc_config())

    def get_id_token(self, *args, **kwargs):
        token = super(LumericalOAuth2Test, self).get_id_token(*args, **kwargs)
        token['userId'] = '123456'
        token['username'] = 'some_user@lumerical.com'
        token['email'] = 'some_user@lumerical.com'
        token['name'] = 'Some User'
        return token

    def test_login(self):
        self.do_login()

    def test_partial_pipeline(self):
        self.do_partial_pipeline()

    # We override the base class method to use JWT encoding for our id_token, because Lumerical doesn't support JWK.
    def prepare_access_token_body(self, client_key=None, tamper_message=False,
                                  expiration_datetime=None,
                                  issue_datetime=None, nonce=None,
                                  issuer=None):
        """
        Prepares a provider access token response. Arguments:

        client_id       -- (str) OAuth ID for the client that requested
                                 authentication.
        expiration_time -- (datetime) Date and time after which the response
                                      should be considered invalid.
        """

        body = {'access_token': 'foobar', 'token_type': 'bearer'}
        client_id, _client_secret = self.backend.get_key_and_secret()
        decryption_key = self.backend.setting('ID_TOKEN_DECRYPTION_KEY')
        client_key = client_key or self.client_key

        now = datetime.datetime.utcnow()
        expiration_datetime = expiration_datetime or \
                              (now + datetime.timedelta(seconds=30))
        issue_datetime = issue_datetime or now
        nonce = nonce or 'a-nonce'
        issuer = issuer or self.issuer
        id_token = self.get_id_token(
            client_key, timegm(expiration_datetime.utctimetuple()),
            timegm(issue_datetime.utctimetuple()), nonce, issuer)

        body['id_token'] = jwt_encode(id_token, decryption_key)

        if tamper_message:
            header, msg, sig = body['id_token'].split('.')
            id_token['sub'] = '1235'
            msg = 'abcdef'
            body['id_token'] = '.'.join([header, msg, sig])

        return json.dumps(body)

class LumericalOAuth2UsernameWithDotTest(LumericalOAuth2Test):
    expected_username = 'namesurname'

    def get_id_token(self, *args, **kwargs):
        token = super(LumericalOAuth2UsernameWithDotTest, self).get_id_token(*args, **kwargs)
        token['userId'] = '123456'
        token['username'] = 'name.surname@lumerical.com'
        token['email'] = 'name.surname@lumerical.com'
        token['name'] = 'Name Surname'
        return token

class LumericalOAuth2UsernameWithPlusSignTest(LumericalOAuth2Test):
    expected_username = 'nametest1'

    def get_id_token(self, *args, **kwargs):
        token = super(LumericalOAuth2UsernameWithPlusSignTest, self).get_id_token(*args, **kwargs)
        token['userId'] = '123456'
        token['username'] = 'name+test1@lumerical.com'
        token['email'] = 'name+test1@lumerical.com'
        token['name'] = 'Some Name'
        return token

class LumericalOAuth2UsernameWithNumbersTest(LumericalOAuth2Test):
    expected_username = '123456'

    def get_id_token(self, *args, **kwargs):
        token = super(LumericalOAuth2UsernameWithNumbersTest, self).get_id_token(*args, **kwargs)
        token['userId'] = '123456'
        token['username'] = '123456@lumerical.com'
        token['email'] = '123456@lumerical.com'
        token['name'] = 'Some Name'
        return token

class LumericalOAuth2UsernameWithDashesTest(LumericalOAuth2Test):
    expected_username = 'some_name-test1'

    def get_id_token(self, *args, **kwargs):
        token = super(LumericalOAuth2UsernameWithDashesTest, self).get_id_token(*args, **kwargs)
        token['userId'] = '123456'
        token['username'] = 'some_name-test1@lumerical.com'
        token['email'] = 'some_name-test16@lumerical.com'
        token['name'] = 'Some Name'
        return token

