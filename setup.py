from setuptools import setup

setup(
    name='lumerical-oauth2',
    version='1.0.0',
    description='python-social-auth backend for Lumerical OAuth2/OpenID',
    packages=['lumerical_oauth2'],
    install_requires=[
        'python-social-auth>=0.2,<0.3'
    ]
)
