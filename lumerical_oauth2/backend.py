import datetime
import logging
import re
from calendar import timegm
from jwt import InvalidTokenError, decode as jwt_decode
from social_core.backends.open_id_connect import OpenIdConnectAuth
from social_core.exceptions import AuthTokenError


log = logging.getLogger(__name__)


class LumericalOAuth2(OpenIdConnectAuth):
    "python-social-auth backend for Lumerical OAuth2/OpenId SSO."

    name = 'lumerical-oauth2'

    AUTHORIZATION_URL = 'https://www.lumerical.com/services/oauth/authorize.php'
    ACCESS_TOKEN_URL = 'https://www.lumerical.com/services/oauth/token.php'
    ACCESS_TOKEN_METHOD = 'POST'
    STATE_PARAMETER = True
    REDIRECT_STATE = False
    DEFAULT_SCOPE = ['openid', 'email', 'profile']

    ID_TOKEN_ISSUER = 'www.lumerical.com'

    # Note: Lumerical does not provide these endpoints; see user_data and oidc_config below.
    OIDC_ENDPOINT = 'http://invalid.oidc-endpoint.com'
    USERINFO_URL = 'http://invalid.oidc-endpoint.com/userinfo/'
    REVOKE_TOKEN_URL = 'http://invalid.oidc-endpoint.com/revoke/'
    JWKS_URI = 'http://invalid.oidc-endpoint.com/jwks/'

    def setting(self, name, default=None):
        if name == 'ID_TOKEN_DECRYPTION_KEY':
            return self.setting('SECRET')
        else:
            return super(LumericalOAuth2, self).setting(name, default)

    def get_user_id(self, details, response):
        return self.id_token['userId']

    def get_user_details(self, response):
        fullname, first_name, last_name = self.get_user_names(fullname=self.id_token['name'])
        username = self.id_token['username'].split('@')[0]
        # Remove unaccepted characters like periods
        # This regexp is based on django.core.validators.slug_re
        username = re.sub(r'[^-a-zA-Z0-9_]','', username)
        return {
            'email': self.id_token['email'],
            'fullname': fullname,
            'first_name': first_name,
            'last_name': last_name,
            'username': username,
        }

    def user_data(self, access_token, *args, **kwargs):
        """
        We have to override this method because Lumerical doesn't provide a USERINFO_URL.
        """
        return self.get_user_details(None)

    def oidc_config(self):
        """
        We have to override this method because Lumerical doesn't provide a OIDC_ENDPOINT.
        """
        return dict(
            issuer=self.ID_TOKEN_ISSUER,
            authorization_endpoint=self.AUTHORIZATION_URL,
            token_endpoint=self.ACCESS_TOKEN_URL,
            userinfo_endpoint=self.USERINFO_URL,
            revocation_endpoint=self.REVOKE_TOKEN_URL,
            jwks_uri=self.JWKS_URI,
        )

    # We have to override validate_and_return_id_token to be able to skip iat_validation
    # on the JWT library level. By default the JWT library will raise an error if iat
    # time is in the future, which can happen if authorization and client server times
    # are not completely in sync.  In python-social-auth version 0.3.*, the check that
    # verifies that iat is not in the future was removed completely, so when we upgrade
    # to 0.3.*, we can remove this overridden method.
    def validate_and_return_id_token(self, id_token):
        """
        Validates the id_token according to the steps at
        http://openid.net/specs/openid-connect-core-1_0.html#IDTokenValidation.
        """
        client_id, _client_secret = self.get_key_and_secret()
        decryption_key = self.setting('ID_TOKEN_DECRYPTION_KEY')

        try:
            # Decode the JWT and raise an error if the secret is invalid or
            # the response has expired.
            id_token = jwt_decode(id_token, decryption_key, audience=client_id,
                                  issuer=self.ID_TOKEN_ISSUER,
                                  options={'verify_iat': False},
                                  algorithms=['HS256'])
        except InvalidTokenError as err:
            log.error("Lumerical SSO got invalid JWT token: %s", id_token)
            raise AuthTokenError(self, err)

        # Verify the token was issued in the last 10 minutes
        utc_timestamp = timegm(datetime.datetime.utcnow().utctimetuple())
        if id_token['iat'] < (utc_timestamp - 600):
            raise AuthTokenError(self, 'Incorrect id_token: iat')

        # Validate the nonce to ensure the request was not modified
        nonce = id_token.get('nonce')
        if not nonce:
            raise AuthTokenError(self, 'Incorrect id_token: nonce')

        nonce_obj = self.get_nonce(nonce)
        if nonce_obj:
            self.remove_nonce(nonce_obj.id)
        else:
            raise AuthTokenError(self, 'Incorrect id_token: nonce')
        return id_token
