# Lumerical OAuth2

This repository contains a python-social-auth based backend for
Lumerical OAuth2/OpenId SSO.

## Installation

To add this backend to you edx-platform installation, you will have to add these
ansible variables:

```yaml
EDXAPP_SOCIAL_AUTH_OAUTH_SECRETS:
  lumerical-oauth2: '<your-secret-key>'

EDXAPP_ENV_EXTRA:
  THIRD_PARTY_AUTH_BACKENDS: ['lumerical_oauth2.backend.LumericalOAuth2']
  # ...

EDXAPP_EXTRA_REQUIREMENTS:
  - name: git+https://gitlab.com/opencraft/client/lumerical/lumerical-oauth2.git@<commit-sha>#egg=lumerical-oauth2
  # - ...
```

## Running Tests

In a fresh virtual environment, first install requirements from `requirements.txt`
and `test_requirements.txt`, then run:

```python
python -m unittest discover tests
```
